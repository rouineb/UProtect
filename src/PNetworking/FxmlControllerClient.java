package PNetworking;

import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FxmlControllerClient extends Application implements Initializable{ 
	
	final int WITH = 650;
	final int HEIGHT = 500;
	public Server server;
	final String MyName="Alice";
	Image _off_notification;
	
	@FXML
	private ImageView imageView;
	
	@FXML
	private Button btnSend;
	
	@FXML 
	private Text title; 
	
	@FXML 
	private TextField editMessage;
	
	@FXML 
	public ListView<String> listView;
	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {

		 
			try {	
				
				   _off_notification= new Image(getClass().getResource("notification_off.png").toString());	
					imageView.setImage(_off_notification);
			
				btnSend.setDefaultButton(true);
				
				
				ConfigNetwork config=new ConfigNetwork();	
				InetAddress inet=null;
					
				System.out.println("\n################### Menu #####################\n");
				System.out.println("1 - Local communication ");	
				System.out.println("2 - Network communication\n");	
				System.out.println("##############################################");

				System.out.print("Enter:");
				int choice = Integer.valueOf( System.console().readLine());
					
					switch(choice){
					
					case 1 : 
					
								inet=InetAddress.getLocalHost();
								
					
						
							break;
					case 2 : System.out.print("Enter server address: ");
								 String ip=System.console().readLine();
								 inet = InetAddress.getByName(ip);
//								if(!config.isReachableAddress(IPSERVER))System.exit(0);
								 config.isReachableAddress(inet);
							break;
							
							
							
					default : System.out.println("Incorrect entry.");	
							  System.exit(0);
					
					}
					
				
					server=new Server(new Socket(inet,Server.PORT),listView,imageView);
					server.start();
			
			} catch(Exception e) {
				System.err.println("Initialize : ");
				e.printStackTrace();
			}

	    }
	
	@FXML 
	void handleSubmitMessage(ActionEvent event){
	
		String value = editMessage.getText();
		
		if(value.length()!=0){
			
			try {
				listView.getItems().add("Moi> "+value);
				editMessage.setText("");
				server.sendMessage(MyName+"> "+value);
				
				imageView.setImage(_off_notification);			

			} catch (Exception e) {
			
				System.err.println("handleSubmitMessage:");
				e.printStackTrace();
			}	
		}	
	}
	
	 @FXML
	void updateNotification(MouseEvent event){
			imageView.setImage(_off_notification);			

	}
	
	@Override
	public void start(Stage primaryStage) {
		try {

			
			Parent root = FXMLLoader.load(getClass().getResource("fxml_client.fxml"));
			Scene scene = new Scene(root,WITH,HEIGHT);
			primaryStage.setTitle("UProtect");
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		
		} catch(Exception e) {
			System.err.println("start : ");
			e.printStackTrace();
		}
	}
	
	
	
	public static void main(String[] args) {
		
		
	launch(args);
				
	}
	
	
	

}
