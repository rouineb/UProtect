package PNetworking;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class ConfigNetwork {


	
	public void  RouterGivenAdress(){
		String IP="";
		 try {
	        	Enumeration<?> e = NetworkInterface.getNetworkInterfaces();
	        	while(e.hasMoreElements())
	        	{
	        	    NetworkInterface n = (NetworkInterface) e.nextElement();
	        	    Enumeration<?> ee = n.getInetAddresses();
	        	    	    
	        	    while (ee.hasMoreElements())
	        	    {
	        	        InetAddress i = (InetAddress) ee.nextElement();
	        	        if (i.isSiteLocalAddress())
	        	        	IP+=i.getHostAddress()+"\n";
	        	    }    
	        	}
	        } catch (Exception e) {
	            System.err.println("RouterGivenAdress: "); e.printStackTrace();
	        }
		 
	        System.out.println((IP.length()==0)?"Check yout internet connexion.":"Launch client with the IP Address: "+IP);

	}
	
		
	
	public Boolean isReachableAddress(InetAddress inet){

		Boolean flag= false;
	    try {
	    	
			flag=inet.isReachable(10);
			System.out.println("Sending Ping Request to " + inet);
			System.out.println(flag ? "Host is reachable" : "Host is NOT reachable");

	    } catch (Exception e) {
			System.err.println("isReachableAddress: ");
	    	e.printStackTrace();
		}
    	return flag;
		
	}
	
	

	
	
}
