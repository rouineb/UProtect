package PNetworking;

import java.net.*;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.*;
import javafx.application.Platform;


public class Server extends Thread{

	public static int PORT=7030;
	public Socket _socket;
	public PrintWriter _out;
	public BufferedReader _in;
	public ListView<String> _listView;
	public ImageView _imageView;
	Image _on_notification;

	
	
	
	@Override
	public void run() {
		
		while(true){
	
				synchronized (_listView) {
					String msg = getMessage();			
					Platform.runLater(() ->_listView.getItems().add(msg));
					_imageView.setImage(_on_notification);
				
				}	
		}
		
	}
	
	
	public Server(Socket socket, ListView<String> listView,ImageView imageView){
		
		try {	
		_socket = socket;
		_listView=listView;
		_imageView=imageView;
		_on_notification = new Image(getClass().getResource("notification_on.png").toString());	

		
		_in=new BufferedReader(new InputStreamReader(_socket.getInputStream()));
		_out = new PrintWriter(_socket.getOutputStream(),true);
		
		} catch (Exception e) {
			
			System.err.println("Construct client : "+e.getLocalizedMessage());
		}
	}
	
	public void sendMessage (String msg) {
		
		try {
			
			_out.println(msg);
			
		} catch (Exception e) {
			
			System.err.println("sendMessage : "+e.getLocalizedMessage());
		}
		
	}
	
	
	public String getMessage(){
	
		try {	
			  return  _in.readLine();
		} catch (Exception e) {
			
			System.err.println("getMessage : "+e.getLocalizedMessage());
			return null;
		}
		
	}
	


}

