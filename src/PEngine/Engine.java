package PEngine;

import java.math.BigInteger;

public abstract class Engine {

	public Engine() {

	}

	public static BigInteger toSend(BigInteger message, Pair<BigInteger, BigInteger> pk) {
		return message.modPow(pk.getSecond(), pk.getFirst());
	}

	public static String toSend(String message, Pair<BigInteger, BigInteger> pk) {
		return (new BigInteger(message.getBytes())).modPow(pk.getSecond(), pk.getFirst()).toString();
	}

}
