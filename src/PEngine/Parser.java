package PEngine;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.math.BigInteger;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;

/*
 * This concerns the UProtect application that ensures the rsa algorithm !
 * */

public class Parser implements IParser {

	protected File mPubKeys;
	protected File mPriKey;
	protected File mHosts;
	public final static String UPROTECT_FILE_SEPARATOR = "============================================================================";
	public static final String HOME_DIRECTORY = System.getProperty("user.home");
	public static final String DEFAULT_LOCATION = HOME_DIRECTORY.concat(File.separator).concat(".UProtect");

	public enum CONFIG_FILES {
		PUBLIC_KEYS, PRIVATE_KEY, KNOWN_HOSTS
	}

	public Parser(ACCESS_MODE pr) {
		if (pr == ACCESS_MODE.DEFAULT_LOCATION) {
			// first let's initialize the files
			mPubKeys = new File(DEFAULT_LOCATION + File.separator + "uprotect_rsa.pub");
			mPriKey = new File(DEFAULT_LOCATION + File.separator + "uprotect_rsa.priv");
			mHosts = new File(DEFAULT_LOCATION + File.separator + "known_hosts");
			// make sure the folder does exist before going any further !
			if (this.init()) {
				// meaning that the folder does exist, we could proceed

			}
		} else if (pr == ACCESS_MODE.TWEAKED_CONFIGURATION) {
			// the user in this case, should create a config file in the same
			// folder
			// in which he states clearly where to find each file

		}
	}

	/*
	 * helps checking if the folder ~/.UProtect exists if not, it will created !
	 * Please note, that this untented to be a hidden folder, which means on
	 * windows it will not work !
	 */
	private boolean init() {
		try {
			File mocker = new File(DEFAULT_LOCATION);
			if (mocker.exists() && mocker.isDirectory()) {
				return true;
			} else {
				// we should create it from the scratch !
				// I wonder it we should try to see of the files are also
				// created !
				mocker.mkdir();
				return _checkFiles();
			}
		} catch (Exception ex) {
			System.err.println("Parser::init() error".concat(ex.getMessage()));
		}
		return false;
	}

	private boolean _checkFiles() {
		try {
			// check if the mandatory files are in place or not

			if (!mPubKeys.exists()) {
				mPubKeys.createNewFile();
			}

			if (!mPriKey.exists()) {
				mPriKey.createNewFile();
			}

			if (!mHosts.exists()) {
				mHosts.createNewFile();
			}
			// everything went ok, yes !
			return true;
		} catch (Exception ex) {
			System.err.println("Parser::checkFiles() ".concat(ex.getMessage()));
			return false;
		}
	}

	/*
	 * This class will be used to parse files, meaning reading & writing into
	 * files
	 */
	@Override
	public Engine load(ACCESS_MODE location) {
		RSAEngine rsa = new RSAEngine();
		try {
			// first let's make it RSAEngine
			if (location == ACCESS_MODE.DEFAULT_LOCATION) {

				FileInputStream fstream = new FileInputStream(mPriKey.getAbsolutePath());
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

				int line = 0;
				String strLine;
				int index = 0;

				Pair<BigInteger, BigInteger> holder = new Pair(BigInteger.ONE, BigInteger.ONE);

				while (index < 2) {
					if (index == 1) {
						fstream = new FileInputStream(mPubKeys.getAbsolutePath());
						br = new BufferedReader(new InputStreamReader(fstream));
					}
					// Read File Line By Line
					while ((strLine = br.readLine()) != null && !strLine.equals(Parser.UPROTECT_FILE_SEPARATOR)) {
						if (line == 0) {
							holder.setFirst(new BigInteger(strLine));
							line++;
						} else {
							holder.setSecond(new BigInteger(strLine));
							if (index == 0)
								rsa.setPrivateKey(holder);
							else
								rsa.setPublicKey(holder);
						}
					}
					index++;
					line = 0;
				}
				// Close the input stream
				br.close();
				fstream.close();
			}

			return rsa;
		} catch (Exception ex) {
			System.err.println("Parser::load() " + ex.getMessage());
			return null;
		}
	}

	public boolean exists(String macaddress) {
		try {
			// Open the file
			FileInputStream fstream = new FileInputStream(mHosts.getAbsolutePath());
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			int line = 0;
			String strLine;

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				if (strLine.equals(Parser.UPROTECT_FILE_SEPARATOR)) {
					line = 0;
				} else if (line == 0 && strLine.equals(macaddress)) {
					return true;
				} else
					line++;
			}

			// Close the input stream
			br.close();
			fstream.close();

			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			return true;
		}
	}

	/*public boolean save(ACCESS_MODE location, CONFIG_FILES type, Pair<BigInteger, BigInteger> pk) {
		try {

			// Open the file
			FileInputStream fstream = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			if (type == CONFIG_FILES.PUBLIC_KEYS) {
				fstream = new FileInputStream(mPubKeys.getAbsolutePath());
			} else if (type == CONFIG_FILES.PRIVATE_KEY) {
				fstream = new FileInputStream(mPriKey.getAbsolutePath());
			} else {
				return false;
			}

			br = new BufferedReader(new InputStreamReader(fstream));
			int line = 0;
			String strLine;
			Pair<BigInteger, BigInteger> holder;

			// Read File Line By Line
			while ((strLine = br.readLine()) != null && !strLine.equals(Parser.UPROTECT_FILE_SEPARATOR)) {
				if (line == 0) {
					
				} else
					line++;
			}

			// Close the input stream
			br.close();
			fstream.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}*/

	/*
	 * save a public key into the knownhosts file
	 */
	@Override
	public boolean save(ACCESS_MODE location, String macAddress, Pair<BigInteger, BigInteger> pk) {
		try {
			if (this.exists(macAddress))
				return false;

			if (location == ACCESS_MODE.DEFAULT_LOCATION) {
				// update the state of the files !
				FileWriter fw = new FileWriter(mHosts.getAbsoluteFile(), true);
				BufferedWriter bw = new BufferedWriter(fw);

				bw.write(macAddress);
				bw.newLine();
				bw.write(pk.getFirst().toString());
				bw.newLine();
				bw.write(pk.getSecond().toString());
				bw.newLine();
				bw.write(Parser.UPROTECT_FILE_SEPARATOR);

				bw.close();
				fw.close();

			} else {
				// not yet implemented
			}
			return true;
		} catch (Exception ex) {
			System.err.println("Parser::save() " + ex.getMessage());
			return false;
		}
	}

}
