package PEngine;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;

import PEngine.IParser.ACCESS_MODE;

public class RSAEngine extends Engine {

	// to skip tedious operations of generating each time two big primes
	Pair<BigInteger, BigInteger> mPubKey;
	Pair<BigInteger, BigInteger> mPriKey;
	int mBitlen;
	// Charset mEncoding;

	private final static SecureRandom mSecure = new SecureRandom();

	public enum ENCRYPT_LENGTH {
		ENC_1024, ENC_2048, ENC_4096
	}

	public RSAEngine() {
	}

	/**
	 * @param compelxity
	 *            : it's the number of bit used to generate the primes ! please
	 *            make sure it does not exceed pow(2,-100)
	 */
	public RSAEngine(int complexity) {
		mBitlen = complexity;
		generateKeys();
	}

	/**
	 * Use a given public key instead of generating our own
	 */
	/*
	 * public RSAEngine(BigInteger n, BigInteger e) { mPubKey = new
	 * Pair<BigInteger, BigInteger>(n, e); }
	 */

	RSAEngine(ENCRYPT_LENGTH choice) {
		if (choice == ENCRYPT_LENGTH.ENC_1024) {
			mBitlen = 1024;
		} else if (choice == ENCRYPT_LENGTH.ENC_2048) {
			mBitlen = 2048;
		} else {
			mBitlen = 4096;
		}
		// _encoding =
		generateKeys();
	}

	public void setPublicKey(Pair<BigInteger, BigInteger> pkey) {
		mPubKey = pkey;
	}

	public void setPrivateKey(Pair<BigInteger, BigInteger> pkey) {
		mPriKey = pkey;
	}

	/**
	 * return the public key
	 */
	public Pair<BigInteger, BigInteger> getPublicKey() {
		return mPubKey;
	}

	/** Encrypt the given plaintext message. */
	public synchronized String encrypt(String message) {
		return (new BigInteger(message.getBytes())).modPow(mPubKey.getSecond(), mPriKey.getFirst()).toString();
	}

	/** Encrypt the given plaintext message. */
	public synchronized BigInteger encrypt(BigInteger message) {
		return message.modPow(mPubKey.getSecond(), mPriKey.getFirst());
	}

	/** Decrypt the given ciphertext message. */
	public synchronized String decrypt(String message) {
		return new String((new BigInteger(message)).modPow(mPriKey.getSecond(), mPubKey.getFirst()).toByteArray());
	}

	/** Decrypt the given ciphertext message. */
	public synchronized BigInteger decrypt(BigInteger message) {
		return message.modPow(mPriKey.getSecond(), mPubKey.getFirst());
	}

	/** Generate a new public and private key set. */
	private synchronized void generateKeys() {
		BigInteger p = new BigInteger(mBitlen / 2, 100, this.mSecure);
		BigInteger q = new BigInteger(mBitlen / 2, 100, this.mSecure);
		BigInteger n = p.multiply(q);
		BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		// BigInteger e = new BigInteger("3");
		this.mPubKey = new Pair<BigInteger, BigInteger>(n, new BigInteger("3"));

		while (m.gcd(this.mPubKey.getSecond()).intValue() > 1) {
			this.mPubKey.setSecond(this.mPubKey.getSecond().add(new BigInteger("2")));
		}
		this.mPriKey = new Pair<BigInteger, BigInteger>(n, this.mPubKey.getSecond().modInverse(m));
	}

	public static void main(String[] args) {

		Parser parser = new Parser(ACCESS_MODE.DEFAULT_LOCATION);

		/*
		 * Please uncomment this section if you want to test
		 */
		//RSAEngine rsa_alice = new RSAEngine(ENCRYPT_LENGTH.ENC_1024);

		// System.out.println(((RSAEngine)parser.load(ACCESS_MODE.DEFAULT_LOCATION)).getPublicKey().getSecond());

		// the problem here is that how to know the public key of the other side
		// !?
		// String text = Engine.toSend("this is just an example !",
		// rsa_alice.getPublicKey());

		// System.out.println(rsa_alice.decrypt(text));

		// System.out.println(parser.save(ACCESS_MODE.DEFAULT_LOCATION,
		// "Kayzen", rsa_alice.getPublicKey()));

		/*
		 * String text1 = "Yellow and Black Border Collies";
		 * System.out.println("Plaintext: " + text1); BigInteger plaintext = new
		 * BigInteger(text1.getBytes());
		 * 
		 * BigInteger ciphertext = rsa.encrypt(plaintext);
		 * System.out.println("Ciphertext: " + ciphertext); plaintext =
		 * rsa.decrypt(ciphertext);
		 * 
		 * String text2 = new String(plaintext.toByteArray());
		 * System.out.println("Plaintext: " + text2);
		 */

	}

}
