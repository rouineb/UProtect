package PEngine;

import java.math.BigInteger;

public interface IParser {
	/*
	 * This function will read in the default location the public key
	 */
	// public boolean readPublicKey();

	/*
	 * The same, reading the private key
	 */
	// public boolean readPrivateKey();

	/*
	 * it will save the file into the default location
	 */
	public boolean save(ACCESS_MODE location, String macAddress, Pair<BigInteger, BigInteger> pk);

	/**
	 * save the private and the public key of an Engine instance
	 */
	// public boolean saveKeys(ACCESS_MODE location, RSAEngine engine);

	/*
	 * read the pair: private & public key
	 */
	public Engine load(ACCESS_MODE location);

	/*
	 * Are we going to use the default configuration or us another tweaked one
	 * ?!
	 */
	enum ACCESS_MODE {
		DEFAULT_LOCATION, TWEAKED_CONFIGURATION, NONE
	}

}
