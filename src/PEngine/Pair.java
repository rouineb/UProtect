package PEngine;

public class Pair<K, V> {

	private K mElement0;
	private V mElement1;

	public static <K, V> Pair<K, V> createPair(K element0, V element1) {
		return new Pair<K, V>(element0, element1);
 	}

	public Pair(K element0, V element1) {
		this.mElement0 = element0;
		this.mElement1 = element1;
	}

	public K getFirst() {
		return this.mElement0;
	}

	public V getSecond() {
		return this.mElement1;	
	}

	public void setFirst(K element) {
		this.mElement0 = element;
	}

	public void setSecond(V element) {
		this.mElement1 = element;
	}

}